from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm


class SingupForm(UserCreationForm):
    """
    Форма для регистрации нового пользователя
    """
    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'username',
                  'email', 'password1', 'password2')


class UpdateUserInfoForm(forms.ModelForm):
    """
    Форма для обновления информации об пользователе
    """
    class Meta:
        model = get_user_model()
        fields = ('username', 'first_name', 'last_name', 'avatar')

    def clean_username(self):
        """Проверяет уникальность поля 'username'."""
        username = self.cleaned_data.get('username')
        user = get_user_model()
        
        if user.objects.filter(username=username).exclude(pk=self.instance.pk).exists():
            raise forms.ValidationError(
                "This username is already in use. Please choose a different one.")
        
        return username

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['avatar'].required = False
