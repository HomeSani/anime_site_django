from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.db.models import QuerySet
from django.utils.http import urlsafe_base64_decode

from anime.models import Anime


class UserRepository:
    """
    Класс для работы с моделью User
    """

    @staticmethod
    def get_by_email(email: str) -> User:
        return get_user_model().objects.get(email=email)

    @staticmethod
    def get_by_pk(pk: int | str) -> User:
        return get_user_model().objects.get(pk=pk)

    @staticmethod
    def get_from_request(request) -> User:
        return request.user

    @classmethod
    def get_by_uid_token(cls, uid: str, token: str) -> User | None:
        user_pk = urlsafe_base64_decode(uid).decode('utf-8')
        user = cls.get_by_pk(user_pk)

        if default_token_generator.check_token(user, token):
            return user

        return None

    @staticmethod
    def get_following_animes(user: User) -> QuerySet[Anime]:
        return user.folowing_animes.prefetch_related('anime__reviews', 'anime__episodes').all()
