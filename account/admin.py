from django.contrib import admin

from account.models import CustomUser


@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    """
    Модель для отображения пользователя в админ панели
    """
    list_display = ('username', 'first_name',
                    'last_name', 'email', 'is_active')
    list_editable = ('is_active', )
