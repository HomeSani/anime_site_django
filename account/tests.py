from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode

from account.models import CustomUser


class AccountTest(TestCase):
    def setUp(self):
        self.client = Client()

        self.test_user = CustomUser.objects.create(
            first_name='Test',
            last_name='Testov',
            username='tester_123',
            email='tester@gmail.com',
            password='12qwQFHB@!$',
        )
        self.test_user.set_password('12qwQFHB@!$')
        self.test_user.save()

    def test_singup(self):
        response = self.client.post(reverse('singup'), {'first_name': 'Pokemon', 'last_name': 'Pokemonov',
                                    'username': 'pokemonn', 'email': 'pokmeon@gmail.com', 'password1': '12qwQFHB@!$', 'password2': '12qwQFHB@!$'})
        new_user_exists = CustomUser.objects.filter(
            email='pokmeon@gmail.com').exists()

        self.assertEqual(response.status_code, 302)
        self.assertTrue(new_user_exists)

    def test_login(self):
        logged_in = self.client.login(
            username=self.test_user.username, password='12qwQFHB@!$')
        self.assertTrue(logged_in)

    def test_logout(self):
        """
        Использование try;execept оправдана выдаваемой ошибкой при get запросе на 'account_detai',
        потому что именно блягодаря ей можно понять вышел ли пользователь
        """
        self.client.force_login(user=self.test_user)

        self.client.logout()

        try:
            self.client.get(reverse('account_detail'))
            self.assertFalse(False)
        except:
            self.assertTrue(True)

    def test_password_change_with_email(self):
        token = default_token_generator.make_token(self.test_user)
        uid = urlsafe_base64_encode(str(self.test_user.pk).encode('utf-8'))

        response = self.client.post(reverse('password_change', kwargs={'uid': uid, 'token': token}), {
                                    'new_password1': '1o2jijjDIJIH!I#!UH#', 'new_password2': '1o2jijjDIJIH!I#!UH#'})
        self.assertEqual(response.status_code, 302)

        logged_in = self.client.login(
            username=self.test_user.username, password='1o2jijjDIJIH!I#!UH#')
        self.assertTrue(logged_in)

    def test_password_change_without_email(self):
        self.test_user.set_password('12qwQFHB@!$')
        self.test_user.save()

        self.client.force_login(user=self.test_user)

        response = self.client.post(reverse('account_detail'), {
                                    'old_password': '12qwQFHB@!$', 'new_password1': 'jhuhuhh2uhuU#@#@U#H!U1x', 'new_password2': 'jhuhuhh2uhuU#@#@U#H!U1x'})
        self.assertEqual(response.status_code, 200)

        logged_in = self.client.login(
            username=self.test_user.username, password='jhuhuhh2uhuU#@#@U#H!U1x')
        self.assertTrue(logged_in)

    def test_user_update_info(self):
        self.client.force_login(user=self.test_user)

        response = self.client.post(reverse('account_detail'), {
                                    'username': 'New_Nick', 'first_name': 'New_tester_name', 'last_name': 'Testov'})

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Данные успешно обнавлены')
