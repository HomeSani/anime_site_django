from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode

from account.forms import UpdateUserInfoForm
from account.repository import UserRepository
from account.tasks import send_email_task


class PasswordResetService:
    """
    Сервис по сбросу пароля
    """
    __slots__ = 'request', 'form'

    def __init__(self, request, form):
        self.form = form
        self.request = request

    def send_mail(self):
        email = self.form.cleaned_data.get('email')
        user = UserRepository.get_by_email(email)
        reset_link = self._build_reset_link(user)
        message = render_to_string('password_reset_email.html', {
                                   'reset_link': reset_link})
        send_email_task.delay(message, email)

    def _build_reset_link(self, user: User) -> str:
        current_site = get_current_site(self.request)
        token = default_token_generator.make_token(user)
        uid = urlsafe_base64_encode(str(user.pk).encode('utf-8'))
        return f'http://{current_site.domain}/accounts/change/{uid}/{token}'


class PasswordChangeService:
    """
    Сервис по изменению пароля
    """
    __slots__ = 'kwargs'

    def __init__(self, kwargs):
        self.kwargs = kwargs

    def get_user_for_kwargs(self) -> User | None:
        uid = self._get_uid()
        token = self._get_token()
        user = UserRepository.get_by_uid_token(uid, token)

        return user

    def _get_uid(self):
        return self.kwargs.get('uid')

    def _get_token(self):
        return self.kwargs.get('token')


class AccountDetailService:
    """
    Сервис по отображению информации об аккаунте
    """
    __slots__ = 'request'

    def __init__(self, request):
        self.request = request

    def get(self) -> dict:
        user = UserRepository.get_from_request(self.request)
        following_animes = UserRepository.get_following_animes(user)

        return self._build_context(user, following_animes)

    def post(self) -> dict:
        context = self.get()
        user = UserRepository.get_from_request(self.request)
        new_context = {}

        if 'old_password' in self.request.POST:
            new_context = self._change_password(user)
        elif 'username' in self.request.POST:
            new_context = self._update_user_info(user)

        return self._update_context(context, new_context)

    def _change_password(self, user: User) -> dict:
        form = PasswordChangeForm(user, self.request.POST)
        new_context = {}

        if form.is_valid():
            form.save()
            new_context['form_password_success'] = True
        else:
            new_context['form_password_errors'] = form.errors

        return new_context

    def _update_user_info(self, user: User) -> dict:
        form = UpdateUserInfoForm(
            self.request.POST, self.request.FILES, instance=user)
        new_context = {}

        if form.is_valid():
            form.save()
            new_context['form_user_update_success'] = True
        else:
            new_context['form_user_update_errors'] = form.errors

        return new_context

    def _update_context(self, ctx: dict, new_ctx: dict) -> dict:
        return {**ctx, **new_ctx}

    def _build_context(self, user, following_animes) -> dict:
        return {
            'user': user,
            'following_animes': following_animes,
        }
