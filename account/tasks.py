from celery import shared_task
from django.core.mail import send_mail
from django.conf import settings


@shared_task()
def send_email_task(message: str, email: str):
    send_mail('Сброс пароля', message, settings.EMAIL_HOST_USER, [email])
