from django.contrib.auth import login, logout
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm, AuthenticationForm
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import FormView


from account.forms import SingupForm
from account.services import PasswordResetService, PasswordChangeService, AccountDetailService


class SingupView(FormView):
    """
    Представление страницы регистрации
    """
    form_class = SingupForm
    template_name = 'singup.html'
    success_url = reverse_lazy('singin')

    def form_valid(self, form) -> HttpResponse:
        form.save()
        return super().form_valid(form)


class SinginView(FormView):
    """
    Представление страницы авторизации
    """
    form_class = AuthenticationForm
    template_name = 'singin.html'
    success_url = reverse_lazy('anime_index')

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return redirect(self.get_success_url())


class PasswordResetView(FormView):
    """
    Представление страницы по сбросу пароля
    """
    form_class = PasswordResetForm
    template_name = 'password_reset.html'
    success_url = reverse_lazy('anime_index')

    def form_valid(self, form):
        service = PasswordResetService(self.request, form)
        service.send_mail()

        return redirect(self.get_success_url())


class PasswordChangeView(FormView):
    """
    Представление страницы по изменению пароля
    """
    form_class = SetPasswordForm
    template_name = 'password_change.html'
    success_url = reverse_lazy('anime_index')

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        service = PasswordChangeService(self.kwargs)
        user = service.get_user_for_kwargs()
        kwargs['user'] = user
        return kwargs

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url())


class AccountDetailView(View):
    """
    Представление подробной информации о пользователе
    """
    def get(self, request):
        service = AccountDetailService(request)
        context = service.get()      
        return render(request, 'account_detail.html', context)

    def post(self, request):
        service = AccountDetailService(request)
        context = service.post()
        return render(request, 'account_detail.html', context)


class LogoutView(View):
    """
    Представление выхода из аккаунта
    """
    def get(self, request):
        logout(request)
        return redirect(reverse('singin'))
