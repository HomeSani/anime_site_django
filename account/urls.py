from django.urls import path

from account.views import SingupView, SinginView, PasswordResetView, PasswordChangeView, AccountDetailView, LogoutView

urlpatterns = [
    path('singup/', SingupView.as_view(), name='singup'),
    path('singin/', SinginView.as_view(), name='singin'),
    path('reset', PasswordResetView.as_view(), name='password_reset'),
    path('detail/', AccountDetailView.as_view(), name='account_detail'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('change/<str:uid>/<str:token>', PasswordChangeView.as_view(), name='password_change'),
]
