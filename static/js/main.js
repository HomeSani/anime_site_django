/*  ---------------------------------------------------
    Theme Name: Anime
    Description: Anime video tamplate
    Author: Colorib
    Author URI: https://colorib.com/
    Version: 1.0
    Created: Colorib
---------------------------------------------------------  */

'use strict';

(function ($) {

    /*------------------
        Preloader
    --------------------*/
    $(window).on('load', function () {
        $(".loader").fadeOut();
        $("#preloder").delay(200).fadeOut("slow");

        /*------------------
            FIlter
        --------------------*/
        $('.filter__controls li').on('click', function () {
            $('.filter__controls li').removeClass('active');
            $(this).addClass('active');
        });
        if ($('.filter__gallery').length > 0) {
            var containerEl = document.querySelector('.filter__gallery');
            var mixer = mixitup(containerEl);
        }
    });

    /*------------------
        Background Set
    --------------------*/
    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    // Search model
    $('.search-switch').on('click', function () {
        $('.search-model').fadeIn(400);
    });

    $('.search-close-switch').on('click', function () {
        $('.search-model').fadeOut(400, function () {
            $('#search-input').val('');
        });
    });

    /*------------------
		Navigation
	--------------------*/
    $(".mobile-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true
    });

    /*------------------
		Hero Slider
	--------------------*/
    var hero_s = $(".hero__slider");
    hero_s.owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        dots: true,
        nav: true,
        navText: ["<span class='arrow_carrot-left'></span>", "<span class='arrow_carrot-right'></span>"],
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        mouseDrag: false
    });

    /*------------------
        Video Player
    --------------------*/
    const player = new Plyr('#player', {
        controls: ['play-large', 'play', 'progress', 'current-time', 'mute', 'captions', 'settings', 'fullscreen', ''],
        seekTime: 25
    });

    /*------------------
        Niceselect
    --------------------*/
    $('select').niceSelect();

    /*------------------
        Scroll To Top
    --------------------*/
    $("#scrollToTopButton").click(function () {
        $("html, body").animate({scrollTop: 0}, "slow");
        return false;
    });

})(jQuery);

function submit() {
    let form = document.getElementsByClassName('rating-area')

    form.submit()
}

function toggle_favorite() {
    let is_follow = document.querySelector('#is_follow').checked;
    document.querySelector('#is_follow').checked = !is_follow;
}

function on_sort_change(selectObj) {
    let value = selectObj.value;
    let currentUrl = window.location.href;
    const match = currentUrl.match(/sort_field=([^&]*)/);

    if (match) {
        currentUrl = currentUrl.replace(match[0], 'sort_field=' + value);
    } else {
        if (currentUrl.includes('?')) {
            currentUrl = currentUrl + '&sort_field=' + value
        } else {
            currentUrl = currentUrl + '?sort_field=' + value
        }
    }

    window.location.replace(currentUrl);
}

function switch_page(linkObj, page_index) {
    let currentUrl = window.location.href;
    const match = currentUrl.match(/page=([^&]*)/);

    if (match) {
        currentUrl = currentUrl.replace(match[0], 'page=' + page_index);
    } else {
        if (currentUrl.includes('?')) {
            currentUrl = currentUrl + '&page=' + page_index
        } else {
            currentUrl = currentUrl + '?page=' + page_index
        }
    }

    window.location.replace(currentUrl);
}


function add_active_tab_parametr(ele) {
    let currentUrl = window.location.href;

    if (!currentUrl.includes('?')) {
        currentUrl = currentUrl + '?active_tab=' + ele.href.split('#')[1]
    } else {
        currentUrl = currentUrl.split('=')[0] + '=' + ele.href.split('#')[1]
    }

    window.history.replaceState({}, null, currentUrl);
}

function replay_review(ele, parent_id) {
    let parent_ele = ele.parentElement;
    let replay_username = parent_ele.querySelector('h5').innerText;
    let textarea = document.querySelector('textarea');
    let parent_id_input = document.querySelector('input[name="parent_id"]')

    textarea.innerText = replay_username + ', ';
    parent_id_input.value = parent_id;

    const y = textarea.getBoundingClientRect().top + window.scrollY;
    window.scroll({
        top: y,
        behavior: 'smooth'
    });
}