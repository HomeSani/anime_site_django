from django.db import models


class Message(models.Model):
    """
    Модель для сообщений обратной связи
    """
    username = models.CharField(max_length=64, verbose_name='Имя пользователя')
    email = models.EmailField(max_length=32, verbose_name='Почта')
    body = models.CharField(max_length=64, verbose_name='Послание')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        
    def __str__(self):
        return f'{self.username} - {self.email}'
