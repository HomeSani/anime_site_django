from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import FormView

from contact.forms import MessageForm


class ContactView(FormView):
    """
    Представление страницы обратной связи
    """
    form_class = MessageForm
    template_name = 'contact.html'
    success_url = reverse_lazy('contact')

    def form_valid(self, form):
        form.save()
        return redirect(self.success_url)
