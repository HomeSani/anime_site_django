from django.forms import ModelForm

from contact.models import Message


class MessageForm(ModelForm):
    """
    Форма для создания сообщений обратной связи
    """
    class Meta:
        model = Message
        fields = '__all__'
