from django.contrib import admin

from contact.models import Message


@admin.register(Message)
class AnimeAdmin(admin.ModelAdmin):
    """
    Модель для отображения сообщений обратной связи
    """
    list_display = ('username', 'email', 'body')
