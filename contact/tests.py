from django.test import TestCase, Client
from django.urls import reverse


class ContactTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_contactview_response(self):
        response = self.client.get(reverse('contact'))
        self.assertEqual(response.status_code, 200)

    def test_contact_message_create(self):
        response = self.client.post(reverse('contact'), {
                               'username': 'Alex', 'email': 'test@mail.ru', 'body': 'Test text'})
        self.assertEqual(response.status_code, 302)
