# Аниме сайт

Мой пет-проект - это платформа для анимешников. Она обеспечивает удобный просмотр аниме-сериалов, позволяет читать посты, связанные с аниме, и быть в курсе последних новостей из этой сферы.

![Главная страница проекта](info_picture.png)

### Cтек технологий:
* Django
* poetry
* Celery
* Postgres
* Redis
* Docker

## Установка

### Зависимости

* git
* [Docker](https://www.docker.com/)
* [DockerCompose](https://docs.docker.com/compose/)

### Клонирование проекта
```console
git clone https://gitlab.com/HomeSani/anime_site_django.git
```

### Настройка проекта
1. Установка переменных окружения:
 
    *   ```console
        cat .env_example > .env
        ```
    * Установите переменные в файле .env:
      * `EMAIL_HOST_USER` - ваша google почта
      * `EMAIL_HOST_PASSWORD` - 16 символьный код доступа, который можно получить по [инструкции](https://mostalony.ru/kak-razreshit-dostup-storonnim-prilozheniyam)
      * `DB_HOST` - на каком хосте будет запущен postgress
      * `DB_NAME` - имя БД
      * `DB_USER` - имя пользователя для БД 
      * `DB_PASSWORD` - пароль для БД
      * `DEBUG` - режим дебага
      * `SECRET_KEY` - секретный ключ приложения
2. Сборка образа:
    ```console
    docker-compose build
    ```
3. Применение миграций:
   ```console
   docker-compose run --rm web sh -c 'poetry run python manage.py migrate'
   ```
4. Применение [фикстур](https://docs.djangoproject.com/en/4.2/topics/db/fixtures/) (Опционально):
   ```console
   docker-compose run --rm web sh -c 'poetry run python manage.py loaddata fixtures.json'
   ```

   Также необходимо скачать и распаковать [медиа файла](https://disk.yandex.ru/d/X0g6MzjY9as-Pg) в папку **media**
5. Создание суперпользователя:
   ```console
   docker-compose run --rm web sh -c 'poetry run python manage.py createsuperuser'
   ```

### Запуск проекта
```console
docker-compose up
```

## Тесты
```console
docker-compose run --rm web sh -c 'poetry run python manage.py test'
```