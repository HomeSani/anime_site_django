from django import template

from anime.repository import CategoryRepository

register = template.Library()


@register.inclusion_tag('header.html', takes_context=True)
def header(context) -> dict:
    return {
        'categories': CategoryRepository.get_all(),
        'context': context,
    }
