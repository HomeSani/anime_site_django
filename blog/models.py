from ckeditor.fields import RichTextField
from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Category(models.Model):
    """
    Модель для категорий
    """
    name = models.CharField(max_length=64, verbose_name='Название')
    slug = models.SlugField(verbose_name='Слаг', unique=True)

    class Meta:
        verbose_name = 'Категорию'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Tag(models.Model):
    """
    Модель для тегов
    """
    name = models.CharField(max_length=64, verbose_name='Название')
    slug = models.SlugField(verbose_name='Слаг', unique=True)

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

    def __str__(self):
        return self.name


class Post(models.Model):
    """
    Модель для постов
    """
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='posts', verbose_name='Автор')
    title = models.CharField(max_length=128, verbose_name='Заголовок')
    slug = models.SlugField(verbose_name='Слаг', unique=True)
    body = RichTextField(null=True, blank=True, verbose_name='Текст',
                         config_name="special", external_plugin_resources=[(
                             'youtube', '/static/shareledge/ckeditor-plugins/youtube/youtube/', 'plugin.js',
                         )])
    categories = models.ManyToManyField(
        Category, related_name='posts', verbose_name='Категории')
    tags = models.ManyToManyField(
        Tag, related_name='posts', verbose_name='Теги')
    poster = models.ImageField(
        upload_to='blog_posters/', verbose_name='Постер')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    def __str__(self):
        return f'{self.author.username} - {self.title}'


class Review(models.Model):
    """
    Модель для отзывов на посты
    """
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='reviews', verbose_name='Автор')
    parent = models.ForeignKey('Review', on_delete=models.CASCADE, related_name='children', verbose_name='Родитель',
                               null=True, blank=True)
    post = models.ForeignKey(
        Post, on_delete=models.CASCADE, related_name='reviews', verbose_name='Пост')
    text = models.CharField(max_length=300, verbose_name='Текст')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    def __str__(self):
        return f'{self.author.username} - {self.post}'
