from django.contrib import admin

from blog.models import Category, Tag, Post, Review


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """
    Модель для отображения категории в админ панели,
    с автозаполнением слага
    """
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    """
    Модель для отображения тега в админ панели,
    с автозаполнением слага
    """
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    """
    Модель для отображения поста в админ панели
    """
    list_display = ('author', 'title', 'get_categories',
                    'get_tags', 'created_at')
    prepopulated_fields = {"slug": ("title",)}

    @admin.display(description='Теги')
    def get_tags(self, obj) -> str:
        if obj.tags.all():
            return list(obj.tags.all().values_list('name', flat=True))
        else:
            return 'NA'

    @admin.display(description='Категории')
    def get_categories(self, obj) -> str:
        if obj.categories.all():
            return list(obj.categories.all().values_list('name', flat=True))
        else:
            return 'NA'


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    """
    Модель для отображения отзыва в админ панели
    """
    list_display = ('author', 'post', 'text')
