from django.db.models import QuerySet

from blog.models import Post, Category, Tag


class PostRepository:
    """
    Класс для работы с моделью Post
    """

    @staticmethod
    def get_by_slug(slug: str) -> Post:
        return Post.objects.get(slug=slug)

    @staticmethod
    def get_all() -> QuerySet[Post]:
        return Post.objects.all()

    @staticmethod
    def filter_by_category_and_tag(queryset: QuerySet[Post], category_slug: str, tag_slug: str) -> QuerySet[Post]:
        if category_slug:
            queryset = queryset.filter(categories__slug=category_slug)
        if tag_slug:
            queryset = queryset.filter(tags__slug=tag_slug)

        return queryset


class CategoryRepository:
    """
    Класс для работы с моделью Category
    """

    @staticmethod
    def get_all() -> QuerySet[Category]:
        return Category.objects.all()


class TagRepository:
    """
    Класс для работы с моделью Tag
    """

    @staticmethod
    def get_all() -> QuerySet[Tag]:
        return Tag.objects.all()
