from django.test import Client, TestCase
from django.urls import reverse

from account.models import CustomUser
from blog.models import Category, Post, Tag


class BlotTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.test_user = CustomUser.objects.create(
            first_name='Test',
            last_name='Testov',
            username='tester_123',
            email='tester@gmail.com',
            password='12qwQFHB@!$',
        )
        
        # Создание тестового поста
        cat = Category.objects.create(name='test_cat', slug='test-cat')
        tag = Tag.objects.create(name='test_tag', slug='test-tag')
        self.test_post = Post.objects.create(
            author=self.test_user,
            title='Test',
            slug='test',
            body='Test text',
            poster='lol.jpg'
        )
        self.test_post.categories.add(cat)
        self.test_post.tags.add(tag)
        self.test_post.save()
    
    def test_review_create(self):
        self.client.force_login(self.test_user)
        response = self.client.post(reverse('post_detail', kwargs={'slug': self.test_post.slug}), {'text': 'Test comment'})

        self.assertEqual(response.status_code, 302)
