from django.urls import path

from blog.views import PostDetailView, PostListView

urlpatterns = [
    path('post/<slug:slug>', PostDetailView.as_view(), name='post_detail'),
    path('posts/', PostListView.as_view(), name='post_list'),
]
