from django import forms

from blog.models import Review


class CreateReviewForm(forms.Form):
    """
    Форма для создания отзыва 
    """
    parent_id = forms.IntegerField(required=False)
    text = forms.CharField(max_length=300)

    def save(self, author, post):
        review = Review.objects.create(
            author_id=author.id,
            post_id=post.id,
            parent_id=self.cleaned_data.get('parent_id'),
            text=self.cleaned_data.get('text')
        )
