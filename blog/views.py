from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import ListView, DetailView

from blog.models import Post
from blog.services import PostDetailService, PostListService


class PostDetailView(DetailView):
    """
    Представление информации о посте
    """
    model = Post
    queryset = Post.objects.prefetch_related('reviews__author', 'reviews__children', 'tags', 'categories')
    context_object_name = 'post'
    template_name = 'post_detail.html'

    def post(self, request, slug):
        service = PostDetailService(request, slug)
        service.post()
        
        return redirect(reverse('post_detail', kwargs={'slug': slug}))


class PostListView(ListView):
    """
    Представление информации постов
    """
    model = Post
    context_object_name = 'posts'
    template_name = 'post_list.html'

    def get_queryset(self):
        service = PostListService(self.request)
        
        return service.get_queryset()

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(PostListView, self).get_context_data()
        service = PostListService(self.request)
        ctx = service.get_context_data()
        context.update(ctx)
        
        return context
