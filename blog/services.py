from django.db.models import QuerySet

from account.repository import UserRepository
from blog.forms import CreateReviewForm
from blog.models import Post
from blog.repository import PostRepository, CategoryRepository, TagRepository


class PostDetailService:
    """
    Сервис для PostDetailView
    """
    __slots__ = 'request', 'slug'

    def __init__(self, request, slug):
        self.request = request
        self.slug = slug

    def post(self) -> None:
        form = CreateReviewForm(self.request.POST)
        author = UserRepository.get_from_request(self.request)
        post = PostRepository.get_by_slug(self.slug)

        if form.is_valid():
            form.save(author, post)


class PostListService:
    """
    Сервис для PostListView
    """
    __slots__ = 'request'

    def __init__(self, request):
        self.request = request

    def get_queryset(self) -> QuerySet[Post]:
        category_slug = self._get_category_slug()
        tag_slug = self._get_tag_slug()
        queryset = PostRepository.get_all()
        queryset = PostRepository.filter_by_category_and_tag(
            queryset, category_slug, tag_slug)

        return queryset

    def get_context_data(self) -> dict:
        categories = CategoryRepository.get_all()
        tags = TagRepository.get_all()
        selected_category = self._get_category_slug()
        selected_tag = self._get_tag_slug()

        return self._build_context(categories, tags, selected_category, selected_tag)

    def _build_context(self, categories, tags, selected_category, selected_tag) -> dict:
        return {
            'categories': categories,
            'tags': tags,
            'selected_category': selected_category,
            'selected_tag': selected_tag,
        }

    def _get_category_slug(self) -> str | None:
        return self.request.GET.get('category', None)

    def _get_tag_slug(self) -> str | None:
        return self.request.GET.get('tag', None)
