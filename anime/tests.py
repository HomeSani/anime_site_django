from datetime import datetime
from django.test import Client, TestCase
from django.urls import reverse
from account.models import CustomUser

from anime.models import Anime, Category, Episode, Genre, Studio


class AnimeTest(TestCase):
    def setUp(self):
        self.client = Client()
        
        self.test_user = CustomUser.objects.create(
            first_name='Test',
            last_name='Testov',
            username='tester_123',
            email='tester@gmail.com',
            password='12qwQFHB@!$',
        )

        # Создание тестового аниме
        studio = Studio.objects.create(name='Test')
        genre = Genre.objects.create(name='test', slug='test-genre')
        cat = Category.objects.create(name='test', slug='test-cat')
        self.test_anime = Anime.objects.create(
            title='tets',
            japan_title='test',
            slug='test-anime',
            description='tets',
            type='test',
            studio=studio,
            start_date=datetime.now(),
            status='test',
            category=cat,
            expected_episode_count=12,
            duration=23,
            views=10,
            quality='test',
            poster='test.jpg'
        )
        self.test_anime.genres.add(genre)
        self.test_anime.save()
        
        self.test_episode = Episode.objects.create(
            title='test',
            slug='test-episode',
            anime=self.test_anime,
            video='test_vidoe.mp4',
            poster='test_poster.jpg'
        )

    def test_anime_review_create(self):
        self.client.force_login(self.test_user)
        response = self.client.post(reverse('anime_detail', kwargs={
                                    'slug': self.test_anime.slug}), {'text': 'Test comment'})
        
        self.assertEqual(response.status_code, 302)

    def test_anime_rating_create(self):
        self.client.force_login(self.test_user)
        response = self.client.post(reverse('anime_detail', kwargs={
                                    'slug': self.test_anime.slug}), {'rating': 3})
        
        self.assertEqual(response.status_code, 302)
        
    def test_anime_following(self):
        self.client.force_login(self.test_user)
        response = self.client.post(reverse('anime_detail', kwargs={
                                    'slug': self.test_anime.slug}), {'is_follow': True})
        
        self.assertEqual(response.status_code, 302)

    def test_episode_review_create(self):
        self.client.force_login(self.test_user)
        response = self.client.post(reverse('anime_watch', kwargs={
                                    'anime_slug': self.test_anime.slug, 'episode_slug': self.test_episode.slug}), {'text': 'Test comment'})
        
        self.assertEqual(response.status_code, 302)
