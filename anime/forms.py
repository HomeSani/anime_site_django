from django import forms

from anime.models import AnimeReview, Vote, EpisodeReview


class AnimeReviewForm(forms.ModelForm):
    """
    Форма для создания комментария для аниме
    """

    class Meta:
        model = AnimeReview
        fields = ('text',)

    def save(self, commit=True, anime_id=None, author_id=None, *args, **kwargs):
        if not anime_id or not author_id:
            raise

        self.instance.author_id = author_id
        self.instance.anime_id = anime_id

        return super().save(commit=commit, *args, **kwargs)


class VoteForm(forms.ModelForm):
    """
    Форма для создания голоса(оценки) для аниме
    """

    class Meta:
        model = Vote
        fields = ('rating',)

    def save(self, commit=True, anime=None, author=None, *args, **kwargs):
        if not anime or not author:
            raise

        self.instance.author = author
        self.instance.anime = anime

        return super().save(commit=commit, *args, **kwargs)


class EpisodeReviewForm(forms.ModelForm):
    """
    Форма для создания комментария для аниме
    """

    class Meta:
        model = EpisodeReview
        fields = ('text',)

    def save(self, commit=True, episode_id=None, author_id=None, *args, **kwargs):
        if not episode_id or not author_id:
            raise

        self.instance.author_id = author_id
        self.instance.episode_id = episode_id

        return super().save(commit=commit, *args, **kwargs)
