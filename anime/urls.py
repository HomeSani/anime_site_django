from django.urls import path

from anime.views import AnimeDetailView, AnimeListView, AnimeWatchView, AnimeIndexView, AnimeSearchResult


urlpatterns = [
    path('', AnimeIndexView.as_view(), name='anime_index'),
    path('animes/<slug:slug>', AnimeListView.as_view(), name='anime_list'),
    path('watch/<slug:anime_slug>/<slug:episode_slug>', AnimeWatchView.as_view(), name='anime_watch'),
    path('search/', AnimeSearchResult.as_view(), name='search_result'),
    path('<slug:slug>/', AnimeDetailView.as_view(), name='anime_detail'),
]
