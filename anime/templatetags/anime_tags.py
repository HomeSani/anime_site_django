from django import template

from anime.models import AnimeReview, Anime
from anime.repository import AnimeRepository

register = template.Library()


# def _get_last_reviewed_animes() -> list:
#     """
#     Возращает 4 недавно откоментированых аниме
#     """
#     reviews = AnimeReview.objects.select_related('anime').order_by('-created_at')
#     animes = []
#
#     tmp = set()
#     for review in reviews:
#         anime = review.anime
#
#         if anime not in tmp:
#             animes.append(anime)
#         tmp.add(anime)
#
#     return animes[:4]


@register.inclusion_tag('tags/sidebar.html')
def sidebar():
    last_reviewed_animes = AnimeRepository.get_last_reviewed()
    more_viewed_animes = AnimeRepository.get_top_viewed()

    context = {
        'last_reviewed_animes': last_reviewed_animes,
        'more_viewed_animes': more_viewed_animes,
    }

    return context


@register.inclusion_tag('tags/large_item.html')
def large_item(anime):
    return {'anime': anime}


@register.inclusion_tag('tags/small_item.html')
def small_item(anime):
    return {'anime': anime}


@register.inclusion_tag('tags/reviews.html', takes_context=True)
def reviews(context, reviews):
    _context = {
        'reviews': reviews,
        'context': context,
    }

    return _context
