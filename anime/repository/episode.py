from django.db.models import QuerySet

from anime.models import Episode


class EpisodeRepository:
    """
    Класс для работы с моделью Episode
    """
    
    @staticmethod
    def get_episodes_by_anime_slug(slug: str) -> QuerySet[Episode]:
        return Episode.objects.filter(anime__slug=slug)

    @staticmethod
    def get_by_anime_and_episode_slug(anime_slug: str, episode_slug: str) -> Episode:
        return Episode.objects.prefetch_related('reviews__author').get(anime__slug=anime_slug, slug=episode_slug)
