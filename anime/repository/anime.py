from datetime import timedelta

from django.contrib.auth.models import User
from django.db.models import QuerySet, Q, Avg
from django.db.models.functions import Coalesce
from django.utils import timezone

from anime import const
from anime.models import Anime, AnimeReview, FollowingAnime


class AnimeRepository:
    """
    Класс для работы с моделью Anime
    """
    
    @staticmethod
    def get_animes() -> QuerySet[Anime]:
        return Anime.objects.prefetch_related('reviews', 'episodes')

    @classmethod
    def get_animes_with_avg_rating(cls) -> QuerySet[Anime]:
        animes = cls.get_animes()
        return animes.annotate(avg_rating=Coalesce(Avg('votes__rating'), 0.0))

    @classmethod
    def get_by_slug(cls, slug: str) -> Anime:
        animes = cls.get_animes()
        return animes.select_related('category', 'studio').get(slug=slug)

    @classmethod
    def get_animes_by_category_slug(cls, slug: str) -> QuerySet[Anime]:
        animes = cls.get_animes()
        return animes.filter(category__slug=slug)

    @staticmethod
    def get_reviews(anime: Anime) -> QuerySet[AnimeReview]:
        return anime.reviews.select_related('author').all()

    @staticmethod
    def get_related_animes(anime: Anime) -> QuerySet[AnimeReview]:
        return Anime.objects.filter(Q(category_id=anime.category.id) | Q(genres__in=anime.genres.all())).exclude(
            id=anime.id).distinct()

    @staticmethod
    def get_rating(anime: Anime) -> float:
        votes = anime.votes.all()
        rating = 0

        for vote in votes:
            rating += vote.rating

        if rating == 0:
            return 0

        return rating / len(votes)

    @staticmethod
    def check_follow(user: User, anime: Anime) -> bool:
        return FollowingAnime.objects.filter(user_id=user.id, anime_id=anime.id).exists()

    @staticmethod
    def order_by_key(queryset: QuerySet[Anime], key: str) -> QuerySet[Anime]:
        match key:
            case const.SORT_BY_VIEWS:
                queryset = queryset.order_by('-views')
            case const.SORT_BY_AZ:
                queryset = queryset.order_by('title')
            case const.SORT_BY_ZA:
                queryset = queryset.order_by('-title')
            case const.SORT_BY_RATING:
                queryset = queryset.annotate(avg_rating=Avg('votes__rating'))
                queryset = queryset.order_by('-avg_rating')
            case const.SORT_BY_NEWSETS:
                queryset = queryset.order_by('created_at')
            case const.SORT_BY_OLDETS:
                queryset = queryset.order_by('-created_at')

        return queryset

    @classmethod
    def get_newsets(cls) -> QuerySet[Anime]:
        animes = cls.get_animes()
        return animes.order_by('-created_at')[:const.INDEX_VIEW_ANIMES_COUNT]

    @classmethod
    def get_featureds(cls) -> QuerySet[Anime]:
        animes = cls.get_animes()
        return animes.filter(is_featured=True)[:const.FEATURED_ANIMES_COUNT]

    @classmethod
    def get_populars_at_season(cls) -> QuerySet[Anime]:
        today = timezone.now()
        three_monts_ago = today - timedelta(days=3 * 30)
        animes = cls.get_animes_with_avg_rating()
        
        return animes.filter(start_date__range=[three_monts_ago, today]).order_by('avg_rating')[
            :const.INDEX_VIEW_ANIMES_COUNT]

    @classmethod
    def get_populars_at_alltime(cls) -> QuerySet[Anime]:
        animes = cls.get_animes_with_avg_rating()
        return animes.order_by('avg_rating')[:const.INDEX_VIEW_ANIMES_COUNT]

    @staticmethod
    def get_last_reviewed() -> QuerySet[Anime]:
        reviews = AnimeReview.objects.select_related(
            'anime').order_by('-created_at')
        animes = []

        tmp = set()
        for review in reviews:
            anime = review.anime

            if anime not in tmp:
                animes.append(anime)
            tmp.add(anime)

        return animes[:const.SIDEBAR_ANIMES_COUNT]

    @classmethod
    def get_top_viewed(cls) -> QuerySet[Anime]:
        return Anime.objects.order_by('views')[:const.SIDEBAR_ANIMES_COUNT]

    @classmethod
    def search_by_phrase(cls, phrase) -> QuerySet[Anime]:
        animes = cls.get_animes()

        if phrase:
            animes = animes.filter(Q(title__iregex=phrase) | Q(description__iregex=phrase) | Q(japan_title__iregex=phrase))

        return animes
