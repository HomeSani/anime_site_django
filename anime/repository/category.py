from django.db.models import QuerySet

from anime import const
from anime.models import Category


class CategoryRepository:
    """
    Класс для работы с моделью Category
    """
    
    @staticmethod
    def get_all() -> QuerySet[Category]:
        return Category.objects.all()

    @staticmethod
    def get_name_by_slug(slug: str) -> str:
        if slug == const.ALL_CATEGORY_SLUG:
            return const.ALL_CATEGORY_NAME
        return Category.objects.get(slug=slug).name

    @staticmethod
    def get_random() -> Category:
        return Category.objects.order_by('?')[0]
