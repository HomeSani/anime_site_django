from .anime import AnimeRepository
from .category import CategoryRepository
from .episode import EpisodeRepository
from .vote import VoteRepository