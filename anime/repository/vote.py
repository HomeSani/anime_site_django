from django.contrib.auth.models import User

from anime.models import Anime, Vote


class VoteRepository:
    """
    Класс для работы с моделью Vote
    """
    
    @staticmethod
    def get_vote(user: User, anime: Anime) -> Vote | None:
        try:
            return anime.votes.get(author_id=user.id)
        except Vote.DoesNotExist:
            return None

    @classmethod
    def get_anime_rating_by_user(cls, user: User, anime: Anime) -> range | int:
        vote = cls.get_vote(user, anime)
        return range(vote.rating + 1) if vote else 0
