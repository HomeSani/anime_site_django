from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from .choices import STATUS_CHOICES, QUALITY_CHOICES, TYPE_CHOICES

User = get_user_model()


class Studio(models.Model):
    """
    Модель для студии
    """
    name = models.CharField(max_length=100, verbose_name='Название')

    class Meta:
        verbose_name = 'Студия'
        verbose_name_plural = 'Студии'

    def __str__(self):
        return self.name


class Genre(models.Model):
    """
    Модель для жанра
    """
    name = models.CharField(max_length=100, verbose_name='Название')
    slug = models.SlugField(unique=True, verbose_name='Слаг')

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    Модель для категории
    """
    name = models.CharField(max_length=100, verbose_name='Название')
    slug = models.SlugField(unique=True, verbose_name='Слаг')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Anime(models.Model):
    """
    Модель для аниме
    """
    title = models.CharField(max_length=200, verbose_name='Название')
    japan_title = models.CharField(max_length=200, verbose_name='Название на японском')
    slug = models.SlugField(unique=True, verbose_name='Слаг')
    description = models.CharField(max_length=500, verbose_name='Описание')
    type = models.CharField(max_length=100, verbose_name='Тип показа', choices=TYPE_CHOICES)
    studio = models.ForeignKey(Studio, on_delete=models.CASCADE, verbose_name='Студия', related_name='animes')
    start_date = models.DateField(verbose_name='Дата начала')
    end_date = models.DateField(verbose_name='Дата окончания', null=True, blank=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, verbose_name='Статус')
    genres = models.ManyToManyField(Genre, verbose_name='Жанры', related_name='animes')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, verbose_name='Категория', related_name='animes')
    expected_episode_count = models.IntegerField(default=0, verbose_name='Ожидаемое кол-во эпизодов')
    duration = models.IntegerField(verbose_name='Длительность эпизода')
    views = models.IntegerField(verbose_name='Просмотры', default=0)
    quality = models.CharField(max_length=10, choices=QUALITY_CHOICES, verbose_name='Качество')
    poster = models.ImageField(upload_to='posters/', verbose_name='Постер')
    poster_for_feature = models.ImageField(upload_to='feature_posters/', verbose_name='Постер для фичинга', blank=True,
                                           null=True)
    is_featured = models.BooleanField(default=False, blank=True, verbose_name='Фичеринг(отображение на главной)')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Аниме'
        verbose_name_plural = 'Аниме'

    def increment_views(self):
        self.views += 1
        self.save(update_fields=['views'])

    def __str__(self):
        return self.title


class Vote(models.Model):
    """
    Модель для голоса
    """
    author = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Автор', related_name='votes')
    anime = models.ForeignKey(Anime, on_delete=models.CASCADE, verbose_name='Аниме', related_name='votes')
    rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], verbose_name='Рейтинг')

    class Meta:
        verbose_name = 'Голос'
        verbose_name_plural = 'Голоса'

    def __str__(self):
        return f'{self.author.username} {self.anime} - {self.rating}'


class FollowingAnime(models.Model):
    """
    Модель для избранного аниме
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь',
                             related_name='folowing_animes')
    anime = models.ForeignKey(Anime, on_delete=models.CASCADE, verbose_name='Аниме')

    class Meta:
        verbose_name = 'Любимое аниме'
        verbose_name_plural = 'Любимые аниме'

    def __str__(self):
        return f'{self.user.username} - {self.anime}'


class Episode(models.Model):
    """
    Модель для эпизодо
    """
    title = models.CharField(max_length=100, verbose_name='Название')
    slug = models.SlugField(verbose_name='Слаг')
    anime = models.ForeignKey(Anime, on_delete=models.CASCADE, related_name='episodes', verbose_name='Аниме')
    video = models.FileField(upload_to='episodes/', verbose_name='Эпизод(видео)')
    poster = models.ImageField(upload_to='episode_posters/', verbose_name='Постер')

    class Meta:
        verbose_name = 'Эпизод'
        verbose_name_plural = 'Эпизоды'

    def __str__(self):
        return f'{self.anime} - {self.title}'


class AnimeReview(models.Model):
    """
    Модель для комментария к аниме
    """
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='anime_reviews', verbose_name='Автор')
    anime = models.ForeignKey(Anime, on_delete=models.CASCADE, related_name='reviews', verbose_name='Аниме')
    text = models.CharField(max_length=300, verbose_name='Текст')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Комментарий к аниме'
        verbose_name_plural = 'Комментарии к аниме'

    def __str__(self):
        return f'{self.author.username} - {self.anime}'


class EpisodeReview(models.Model):
    """
       Модель для комментария к эпизоду
    """
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='episode_reviews', verbose_name='Автор')
    episode = models.ForeignKey(Episode, on_delete=models.CASCADE, related_name='reviews', verbose_name='Эпизод')
    text = models.CharField(max_length=300, verbose_name='Текст')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    class Meta:
        verbose_name = 'Комментарий к эпизоду'
        verbose_name_plural = 'Комментарии к эпизодам'

    def __str__(self):
        return f'{self.author.username} - {self.episode}'
