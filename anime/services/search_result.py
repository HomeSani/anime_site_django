from anime.repository import AnimeRepository


class SearchResultService:
    """
    Сервис для SearchResultView
    """
    __slots__ = 'request'

    def __init__(self, request):
        self.request = request

    def get_queryset(self):
        phrase = self._get_search_phrase()
        key = self._get_sort_field()
        
        queryset = AnimeRepository.search_by_phrase(phrase)
        queryset = AnimeRepository.order_by_key(queryset, key)
        
        return queryset
    
    def get_context(self) -> dict:
        phrase = self._get_search_phrase()
        key = self._get_sort_field()
        return self._build_context(key, phrase)

    def _get_search_phrase(self) -> str:
        return self.request.GET.get('p', None)
    
    def _get_sort_field(self) -> str | None:
        return self.request.GET.get('sort_field', None)

    def _build_context(self, key:str, phrase: str) -> dict:
        return {
            'filter_by': key,
            'search_phrase': phrase,
        }
