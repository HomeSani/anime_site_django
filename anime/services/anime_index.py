from anime.repository import AnimeRepository, CategoryRepository


class AnimeIndexService:
    """
    Сервис для AnimeIndexView
    """
    __slots__ = 'request', 'kwargs'

    def __init__(self, request, kwargs):
        self.kwargs = kwargs
        self.request = request

    def get_context(self):
        random_category = CategoryRepository.get_random()
        popular_at_season = AnimeRepository.get_populars_at_season()
        popular_at_alltime = AnimeRepository.get_populars_at_alltime()
        newsets = AnimeRepository.get_newsets()
        featureds = AnimeRepository.get_featureds()
        animes_by_category = AnimeRepository.get_animes_by_category_slug(
            random_category.slug)

        return self._build_context(random_category, popular_at_season, popular_at_alltime, newsets, featureds,
                                   animes_by_category)

    def _build_context(self, random_category, popular_at_season, popular_at_alltime, newsets, featureds,
                       animes_by_category):
        return {
            'season_trand_animes': popular_at_season,
            'all_time_trand_animes': popular_at_alltime,
            'newset_animes': newsets,
            'category_animes': animes_by_category,
            'featured_animes': featureds,
            'category': random_category,
        }
