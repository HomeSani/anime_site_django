from anime.forms import EpisodeReviewForm
from anime.repository import EpisodeRepository
from account.repository import UserRepository


class AnimeWatchService:
    """
    Сервис для AnimeWatchView
    """
    __slots__ = 'request', 'kwargs'

    def __init__(self, request, kwargs):
        self.kwargs = kwargs
        self.request = request

    def get_object(self):
        anime_slug = self._get_anime_slug()
        episode_slug = self._get_episode_slug()
        return EpisodeRepository.get_by_anime_and_episode_slug(anime_slug, episode_slug)

    def get_context(self):
        episodes = self._get_episodes()
        return self._build_context(episodes)

    def create_episode_review(self):
        form = EpisodeReviewForm(self.request.POST)
        episode = self.get_object()
        user = UserRepository.get_from_request(self.request)

        if form.is_valid():
            form.save(episode_id=episode.id, author_id=user.id)

    def _get_anime_slug(self):
        return self.kwargs.get('anime_slug')

    def _get_episode_slug(self):
        return self.kwargs.get('episode_slug')

    def _get_episodes(self):
        anime_slug = self._get_anime_slug()
        return EpisodeRepository.get_episodes_by_anime_slug(anime_slug)

    def _build_context(self, episodes):
        return {
            'episodes': episodes
        }
