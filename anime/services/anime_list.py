from anime.const import ALL_CATEGORY_SLUG
from anime.repository import AnimeRepository, CategoryRepository


class AnimeListService:
    """
    Сервис для AnimeListView
    """
    __slots__ = 'request', 'kwargs'

    def __init__(self, request, kwargs):
        self.kwargs = kwargs
        self.request = request

    def _get_category_slug(self) -> None:
        return self.kwargs.get('slug')

    def _get_sort_field(self) -> None:
        return self.request.GET.get('sort_field', None)

    def _build_context(self, key, category_name):
        return {
            'filter_by': key,
            'category_name': category_name,
        }

    def get_queryset(self):
        category_slug = self._get_category_slug()
        key = self._get_sort_field()
        queryset = AnimeRepository.get_animes()

        if category_slug != ALL_CATEGORY_SLUG:
            queryset = AnimeRepository.get_animes_by_category_slug(
                category_slug)

        queryset = AnimeRepository.order_by_key(queryset, key)

        return queryset

    def get_context(self):
        category_slug = self._get_category_slug()
        category_name = CategoryRepository.get_name_by_slug(category_slug)
        key = self._get_sort_field()

        return self._build_context(key, category_name)
