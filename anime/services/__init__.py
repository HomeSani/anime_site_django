from .anime_detail import AnimeDetailService
from .anime_list import AnimeListService
from .anime_watch import AnimeWatchService
from .anime_index import AnimeIndexService
from .search_result import SearchResultService