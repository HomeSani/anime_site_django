from django.contrib.auth.models import User

from anime.forms import AnimeReviewForm, VoteForm
from anime.models import Anime, Vote, FollowingAnime
from anime.repository import AnimeRepository, VoteRepository
from account.repository import UserRepository


class AnimeDetailService:
    """
    Сервис для AnimeDetailView
    """
    __slots__ = 'request', 'slug'

    def __init__(self, request, slug):
        self.request = request
        self.slug = slug

    def get(self):
        anime = AnimeRepository.get_by_slug(self.slug)
        reviews = AnimeRepository.get_reviews(anime)
        related_animes = AnimeRepository.get_related_animes(anime)
        rating = AnimeRepository.get_rating(anime)
        user = UserRepository.get_from_request(self.request)
        rating_by_user = VoteRepository.get_anime_rating_by_user(user, anime)
        is_follow = AnimeRepository.check_follow(user, anime)

        anime.increment_views()

        return self._build_context(anime, reviews, related_animes, rating, rating_by_user, is_follow)

    def post(self):
        anime = AnimeRepository.get_by_slug(self.slug)
        user = UserRepository.get_from_request(self.request)

        if self.request.POST.get('text'):
            self._create_anime_review(user, anime)
        elif self.request.POST.get('rating'):
            self._create_or_update_vote(user, anime)
        else:
            self._follow_or_unfollow_anime(user, anime)

    def _build_context(self, anime, reviews, related_animes, rating, rating_by_user, is_follow):
        return {
            'anime': anime,
            'reviews': reviews,
            'related_animes': related_animes,
            'rating': rating,
            'user_rating': rating_by_user,
            'is_follow': is_follow,
        }

    def _create_anime_review(self, user: User, anime: Anime) -> None:
        form = AnimeReviewForm(self.request.POST)

        if form.is_valid():
            form.save(anime_id=anime.id, author_id=user.id)

    def _create_or_update_vote(self, user: User, anime: Anime) -> None:
        form = VoteForm(self.request.POST)

        if form.is_valid():
            Vote.objects.update_or_create(anime_id=anime.id, author_id=user.id,
                                          defaults={'rating': form.cleaned_data.get('rating')})

    def _follow_or_unfollow_anime(self, user: User, anime: Anime) -> None:
        if not self.request.POST.get('is_follow', False):
            FollowingAnime.objects.get(
                user_id=user.id, anime_id=anime.id).delete()
        else:
            FollowingAnime.objects.create(user_id=user.id, anime_id=anime.id)
