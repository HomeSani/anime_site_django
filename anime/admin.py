from django.contrib import admin

from anime.models import Anime, Category, Genre, Episode, Studio, Vote, FollowingAnime, AnimeReview, EpisodeReview


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """
    Модель для отображения категории в админ панели,
    с автозаполнением слага
    """
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    """
    Модель для отображения жанра в админ панели,
    с автозаполнением слага
    """
    prepopulated_fields = {"slug": ("name",)}


@admin.register(Anime)
class AnimeAdmin(admin.ModelAdmin):
    """
    Модель для отображения аниме в админ панели,
    с автозаполнением слага
    """
    list_display = ('title', 'japan_title', 'category', 'views')
    prepopulated_fields = {"slug": ("title",)}
    exclude = ('views',)


@admin.register(Episode)
class EpisodeAdmin(admin.ModelAdmin):
    """
    Модель для отображения эпизода в админ панели,
    с автозаполнением слага
    """
    prepopulated_fields = {"slug": ("title",)}


@admin.register(AnimeReview)
class AnimeReviewAdmin(admin.ModelAdmin):
    """
    Модель для отображения отзыва на аниме в админ панели
    """
    list_display = ('author', 'anime', 'text', 'created_at')


@admin.register(EpisodeReview)
class EpisodeReviewAdmin(admin.ModelAdmin):
    """
    Модель для отображения отзыва на эпизод в админ панели
    """
    list_display = ('author', 'episode', 'text', 'created_at')


@admin.register(FollowingAnime)
class FollowingAnimeAdmin(admin.ModelAdmin):
    """
    Модель для отображения избранных аниме в админ панели
    """
    list_display = ('user', 'anime')


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    """
    Модель для отображения оценок в админ панели
    """
    list_display = ('author', 'anime', 'rating')


admin.site.register(Studio)
