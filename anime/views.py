from django.shortcuts import redirect, render
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, DetailView, TemplateView

from anime.models import Anime, Episode
from anime.services import AnimeDetailService, AnimeListService, AnimeWatchService, AnimeIndexService, SearchResultService


class AnimeDetailView(View):
    """
    Представление детальной информации о аниме
    """
    def get(self, request, slug):
        service = AnimeDetailService(request, slug)
        context = service.get()
        return render(request, 'anime_detail.html', context)

    def post(self, request, slug):
        service = AnimeDetailService(request, slug)
        service.post()
        return redirect(reverse('anime_detail', kwargs={'slug': slug}))


class AnimeListView(ListView):
    """
    Представление всех аниме по категории
    Фильтрация аниме по ?sort_field=
    """
    model = Anime
    template_name = 'anime_list.html'
    paginate_by = 12

    def get_queryset(self):
        service = AnimeListService(self.request, self.kwargs)
        queryset = service.get_queryset()
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(AnimeListView, self).get_context_data(**kwargs)
        service = AnimeListService(self.request, self.kwargs)
        ctx = service.get_context()
        context.update(ctx)
        return context


class AnimeWatchView(DetailView):
    """
    Представление детальной информации о эпизоде
    """
    model = Episode
    slug_url_kwarg = 'episode_slug'
    template_name = 'anime_watch.html'
    context_object_name = 'episode'

    def get_object(self, queryset=None):
        service = AnimeWatchService(self.request, self.kwargs)
        return service.get_object()

    def get_context_data(self, **kwargs):
        context = super(AnimeWatchView, self).get_context_data(**kwargs)
        service = AnimeWatchService(self.request, self.kwargs)
        ctx = service.get_context()
        context.update(ctx)
        return context

    def post(self, request, anime_slug, episode_slug):
        service = AnimeWatchService(self.request, self.kwargs)
        service.create_episode_review()
        return redirect(reverse('anime_watch', kwargs={'anime_slug': anime_slug, 'episode_slug': episode_slug}))


class AnimeIndexView(TemplateView):
    """
    Представление главной страницы
    """

    template_name = 'anime_index.html'

    def get_context_data(self, **kwargs):
        context = super(AnimeIndexView, self).get_context_data()
        service = AnimeIndexService(self.request, self.kwargs)
        ctx = service.get_context()
        context.update(ctx)
        return context


class AnimeSearchResult(ListView):
    """
    Представление результатов поиска
    """
    model = Anime
    template_name = 'search_result.html'
    paginate_by = 12

    def get_queryset(self):
        service = SearchResultService(self.request)
        queryset = service.get_queryset()
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        service = SearchResultService(self.request)
        ctx = service.get_context()
        context.update(ctx)
        return context
