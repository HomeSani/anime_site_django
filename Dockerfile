FROM python:3.11
ENV PYTHONBUMBUFFERED=1
WORKDIR /code
COPY pyproject.toml /code/
RUN pip install poetry
RUN poetry install
COPY . /code/
